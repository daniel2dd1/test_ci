import org.example.calculadora;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class calculatorTest {
    @org.junit.jupiter.api.Test
    void add1(){
        assertEquals(3, calculadora.add(1,2));
        assertEquals(-2, calculadora.add(-5,3));
    }

    @org.junit.jupiter.api.Test
    void compare1() {
        assertEquals(2, calculadora.compare(1, 2));
        assertEquals(1011, calculadora.compare(-9000, 1011));
    }

    @org.junit.jupiter.api.Test
    void mult1() {
        assertEquals(2, calculadora.mult(1, 2));
        assertEquals(50, calculadora.mult(5, 10));
    }

    @org.junit.jupiter.api.Test
    void sub1() {
        assertEquals(0, calculadora.sub(2, 2));
        assertEquals(-30, calculadora.sub(-40, -10));
    }

    @org.junit.jupiter.api.Test
    void div1() {
        assertEquals(0.5, calculadora.div(1, 2));
        assertEquals(5, calculadora.div(25, 5));
    }
}
